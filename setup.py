from setuptools import setup

setup(name='lottery_calc',
      version='0.1',
      description='Calculates your lottery winnings from a JSON file of lottery ticket numbers',
      url='',
      author='Nathan Crum',
      author_email='nathan.crum@outlook.com',
      license='MIT',
      packages=['lottery_calc'],
      zip_safe=False)
