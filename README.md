# Lottery Calculator
This module will help you calculate your Mega Millions lottery winnings from a JSON file of lottery ticket numbers. It is especially useful for large office lottery pool where a large number of tickets were purchased and need verifying.

## Install
```
$ cd /path/to/folder/for/source
$ git clone git@gitlab.com:crumdev/lottery_calc.git .
$ python -m pip install .
```

## Example Input JSON file
```json
{
    "Tickets": [
        [
            "20",
            "30",
            "46",
            "50",
            "70",
            "12"
        ],
        [
            "24",
            "31",
            "34",
            "52",
            "55",
            "10"
        ],
        [
            "05",
            "28",
            "46",
            "50",
            "70",
            "24"
        ]
    ]
}
```

## Example Use
```python
import lottery_calc as lc

lc.current_jackpot = 1537000000
lc.winning_numbers = ["05", "28", "62", "65", "70", "05"]
lc.path_to_input = "tickets.json"

tickets = lc.load_json(lc.path_to_input)

lc.total_winnings(tickets)
```

## Example Output
```
['05', '28', '46', '50', '70', '24']
Matches:  3
Jackpot # match:  False
Ticket Winnings:  $10.00
------------------------------------
Total winnings:  $10.00
```