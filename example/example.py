import lottery_calc as lc

lc.current_jackpot = 1537000000
lc.winning_numbers = ["05", "28", "62", "65", "70", "05"]
lc.path_to_input = "tickets.json"

tickets = lc.load_json(lc.path_to_input)

lc.total_winnings(tickets)