import unittest
import lottery_calc as lc


class TestLotteryCalcMethods(unittest.TestCase):
    lc.current_jackpot = 40000000
    lc.winning_numbers = ["05", "28", "62", "65", "70", "05"]

    #
    # 5 matches && Jackpot == current_jackpot
    # 5 matches ! Jackpot == $1,000,000
    # 4 matches && Jackpot == $10,000
    # 4 matches ! Jackpot == $500
    # 3 matches && Jackpot == $200
    # 3 matches ! Jackpot == $10
    # 2 matches && Jackpot == $10
    # 1 matches && Jackpot == $4
    # 0 matches && Jackpot == $2
    # Else == $0
    #

    lc.path_to_input = "./tests/test_input.json"

    tickets = lc.load_json(lc.path_to_input)

    def test_five_matches_and_jackpot(self):
        self.assertEqual(40000000, lc.reward_calc(5, True))

    def test_five_matches_no_jackpot(self):
        self.assertEqual(1000000, lc.reward_calc(5, False))

    def test_four_matches_and_jackpot(self):
        self.assertEqual(10000, lc.reward_calc(4, True))

    def test_four_matches_no_jackpot(self):
        self.assertEqual(500, lc.reward_calc(4, False))

    def test_three_matches_and_jackpot(self):
        self.assertEqual(200, lc.reward_calc(3, True))

    def test_three_matches_no_jackpot(self):
        self.assertEqual(10, lc.reward_calc(3, False))

    def test_two_matches_and_jackpot(self):
        self.assertEqual(10, lc.reward_calc(2, True))

    def test_two_matches_no_jackpot(self):
        self.assertEqual(0, lc.reward_calc(2, False))

    def test_one_match_and_jackpot(self):
        self.assertEqual(4, lc.reward_calc(1, True))

    def test_one_match_no_jackpot(self):
        self.assertEqual(0, lc.reward_calc(1, False))

    def test_no_matches_and_jackpot(self):
        self.assertEqual(2, lc.reward_calc(0, True))

    def test_no_matches_no_jackpot(self):
        self.assertEqual(0, lc.reward_calc(0, False))

    def test_sum_of_winnings(self):
        self.assertEqual(41010726, lc.total_winnings(self.tickets))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestLotteryCalcMethods)
    unittest.TextTestRunner(verbosity=0).run(suite)
