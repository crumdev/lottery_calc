import json
import locale


locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
winning_numbers = []
"""List of 5 winning lottery picks plus the jackpot number"""

current_jackpot = 1537000000
"""Current total possible winnings"""

path_to_input = "tickets.json"
"""Current total possible winnings"""


def reward_calc(matches, jackpot):
    """
    Processes a single array of lottery picks and returns any winnings in addition to printing them to the console.

    Parameters
    ----------
    matches : int
        The number of matches in your lottery pick for the first 5 numbers
    jackpot : bool
        A bool that determines if the jackpot number matches the winning jackpot number 
    
    >>> reward_calc(3, False)
    10
    """
    winnings = 0
    if matches == 5 and jackpot:
        winnings = current_jackpot
    elif matches == 5 and not jackpot:
        winnings = 1000000
    elif matches == 4 and jackpot:
        winnings = 10000
    elif matches == 4 and not jackpot:
        winnings = 500
    elif matches == 3 and jackpot:
        winnings = 200
    elif matches == 3 and not jackpot:
        winnings = 10
    elif matches == 2 and jackpot:
        winnings = 10
    elif matches == 1 and jackpot:
        winnings = 4
    elif matches == 0 and jackpot:
        winnings = 2
    else:
        pass

    print("")
    print("Matches: ", matches)
    print("Jackpot # match: ", jackpot)
    print("Ticket Winnings: ", (locale.currency(winnings, grouping=True)))
    return winnings


def total_winnings(json_input):
    """
    Loop over tickets and prints the total winnings

    Parameters
    ----------
    json_input : json encoded string or file
        This file should have one key "Tickets" that contains an list of lists.
        Each list will have the 5 Mega Millions lottery picks first then Jackpot number
        formatted as strings e.g. ["05","26","10","70","55","24"]
    """

    sum_of_winnings = 0

    for ticket in json_input["Tickets"]:
        matching_picks = 0
        jackpot = False

        for pick in ticket[:5]:
            if pick in winning_numbers:
                matching_picks += 1

        if ticket[5] == winning_numbers[5]:
            jackpot = True

        if matching_picks > 2 or jackpot:
            print(ticket)
            sum_of_winnings += reward_calc(matching_picks, jackpot)
    
    print("------------------------------------")
    print("Total winnings: ", locale.currency(sum_of_winnings, grouping=True))
    return sum_of_winnings


def load_json(file):
    with open(file, "r") as read_file:
        data = json.load(read_file)
    return data


if __name__ == "__main__":
    """Just an example of how to use the code"""

    winning_numbers = ["01", "23", "55", "69", "44", "25"]
    total_winnings(load_json("tickets.json"))
